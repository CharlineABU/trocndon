package tdb.grp3.trocndon.form;

import lombok.Data;

@Data
public class ReponseForm {

    private Long   idQuestion;
    private String reponse;
    private String question;

    public ReponseForm() {
        // TODO Stub du constructeur généré automatiquement
    }

    public Long getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(Long idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getReponse() {
        return reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

}
