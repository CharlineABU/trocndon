package tdb.grp3.trocndon.form;

import lombok.Data;

@Data
public class ContactezTrockeurForm {

    Long   id;
    String objet;
    String message;

    public ContactezTrockeurForm() {
        // TODO Stub du constructeur généré automatiquement
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
