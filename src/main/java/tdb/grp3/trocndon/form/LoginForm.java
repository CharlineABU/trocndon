package tdb.grp3.trocndon.form;

import lombok.Data;

@Data
public class LoginForm {

    private String email;
    private String password;

    public LoginForm() {
        // TODO Stub du constructeur généré automatiquement
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
