package tdb.grp3.trocndon.form;

import java.util.Date;

import lombok.Data;

@Data
public class TrocForm {

    private String titre;
    private String type;
    private Date   dateAction;
    private String description;

    public TrocForm() {
        // TODO Stub du constructeur généré automatiquement
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDateAction() {
        return dateAction;
    }

    public void setDateAction(Date dateAction) {
        this.dateAction = dateAction;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
