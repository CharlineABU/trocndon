package tdb.grp3.trocndon.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@ConfigurationProperties("email")
@Data
public class EmailProperties {

    private String  mailHost;
    private Integer mailPort;

    private String  mailUserName;
    private String  mailPassword;

    private String  mailNoReplay;
    private String  mailReplay;

    private String  mailTransportProtocol;
    private String  mailSmtpAuth;
    private String  mailSmtpStartTlsEnable;
    private String  mailDebug;

    public EmailProperties() {
        // TODO Stub du constructeur généré automatiquement
    }

    public String getMailHost() {
        return mailHost;
    }

    public void setMailHost(String mailHost) {
        this.mailHost = mailHost;
    }

    public Integer getMailPort() {
        return mailPort;
    }

    public void setMailPort(Integer mailPort) {
        this.mailPort = mailPort;
    }

    public String getMailUserName() {
        return mailUserName;
    }

    public void setMailUserName(String mailUserName) {
        this.mailUserName = mailUserName;
    }

    public String getMailPassword() {
        return mailPassword;
    }

    public void setMailPassword(String mailPassword) {
        this.mailPassword = mailPassword;
    }

    public String getMailNoReplay() {
        return mailNoReplay;
    }

    public void setMailNoReplay(String mailNoReplay) {
        this.mailNoReplay = mailNoReplay;
    }

    public String getMailReplay() {
        return mailReplay;
    }

    public void setMailReplay(String mailReplay) {
        this.mailReplay = mailReplay;
    }

    public String getMailTransportProtocol() {
        return mailTransportProtocol;
    }

    public void setMailTransportProtocol(String mailTransportProtocol) {
        this.mailTransportProtocol = mailTransportProtocol;
    }

    public String getMailSmtpAuth() {
        return mailSmtpAuth;
    }

    public void setMailSmtpAuth(String mailSmtpAuth) {
        this.mailSmtpAuth = mailSmtpAuth;
    }

    public String getMailSmtpStartTlsEnable() {
        return mailSmtpStartTlsEnable;
    }

    public void setMailSmtpStartTlsEnable(String mailSmtpStartTlsEnable) {
        this.mailSmtpStartTlsEnable = mailSmtpStartTlsEnable;
    }

    public String getMailDebug() {
        return mailDebug;
    }

    public void setMailDebug(String mailDebug) {
        this.mailDebug = mailDebug;
    }

}
