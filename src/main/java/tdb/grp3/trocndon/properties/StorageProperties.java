package tdb.grp3.trocndon.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {

	private String locationDir;

	public String getLocationDir() {
		return locationDir;
	}

	public void setLocationDir(String locationDir) {
		this.locationDir = locationDir;
	}

}