package tdb.grp3.trocndon.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@SequenceGenerator(name = "sequence_troc", initialValue = 1, allocationSize = 1)
@Data
@NoArgsConstructor
public class Troc {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_troc")
    private Long   id;
    private String titre;
    private String type;
    @Temporal(TemporalType.DATE)
    private Date   dateAction;
    private String description;

    private String photoTroc;

    @ManyToOne()
    private Membre donneur;

    public Troc() {
        // TODO Stub du constructeur généré automatiquement
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDateAction() {
        return dateAction;
    }

    public void setDateAction(Date dateAction) {
        this.dateAction = dateAction;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoTroc() {
        return photoTroc;
    }

    public void setPhotoTroc(String photoTroc) {
        this.photoTroc = photoTroc;
    }

    public Membre getDonneur() {
        return donneur;
    }

    public void setDonneur(Membre donneur) {
        this.donneur = donneur;
    }

}
