package tdb.grp3.trocndon.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@SequenceGenerator(name = "sequence_membre", initialValue = 1, allocationSize = 1)
@Data
@NoArgsConstructor
public class Membre {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_membre")
    private Long   id;
    @NonNull
    private String nom;
    @NonNull
    private String prenom;
    @Temporal(TemporalType.DATE)
    @NonNull
    private Date   dateNaissance;
    @NonNull
    private String sexe;
    @OneToOne(mappedBy = "membre")
    private User   user;

    //	@OneToMany(mappedBy="donneur", fetch=FetchType.EAGER)
    //	private List<Troc> trocs;
    
    public Membre() {
        // TODO Stub du constructeur généré automatiquement
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    
}
