package tdb.grp3.trocndon.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@SequenceGenerator(name = "sequence_question", initialValue = 1, allocationSize = 1)
@Data
@NoArgsConstructor
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_question")
    private Long          id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date          dateQuestion;
    private String        question;

    @ManyToOne
    private Membre        poseur;

    @OneToMany(mappedBy = "question")
    private List<Reponse> reponses;

    public Question() {
        // TODO Stub du constructeur généré automatiquement
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateQuestion() {
        return dateQuestion;
    }

    public void setDateQuestion(Date dateQuestion) {
        this.dateQuestion = dateQuestion;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Membre getPoseur() {
        return poseur;
    }

    public void setPoseur(Membre poseur) {
        this.poseur = poseur;
    }

    public List<Reponse> getReponses() {
        return reponses;
    }

    public void setReponses(List<Reponse> reponses) {
        this.reponses = reponses;
    }

}
