package tdb.grp3.trocndon.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@SequenceGenerator(name = "sequence_reponse", initialValue = 1, allocationSize = 1)
@Data
@NoArgsConstructor
public class Reponse {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_reponse")
	private Long id;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateReponse;
	private String reponse;

	@ManyToOne()
	private Membre repondeur;
	@ManyToOne()
	private Question question;
	
	public Reponse() {
        // TODO Stub du constructeur généré automatiquement
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateReponse() {
        return dateReponse;
    }

    public void setDateReponse(Date dateReponse) {
        this.dateReponse = dateReponse;
    }

    public String getReponse() {
        return reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    public Membre getRepondeur() {
        return repondeur;
    }

    public void setRepondeur(Membre repondeur) {
        this.repondeur = repondeur;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
	
	

}
