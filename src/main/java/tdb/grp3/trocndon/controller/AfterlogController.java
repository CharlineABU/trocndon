package tdb.grp3.trocndon.controller;


import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tdb.grp3.trocndon.dao.TrocDao;
import tdb.grp3.trocndon.model.Troc;



@Controller
public class AfterlogController {
	
	@Autowired
	private TrocDao trocDao;
	
	@GetMapping("/afterlog")
	public String afficherAcceuil(Model model) {
		
		List<Troc> trocs = trocDao.findAll();
		model.addAttribute("trocs", trocs);
		
		return "afterlog";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = { "/mestrocks" })
	public String mestrocks(Model model) {
		
		return "/doAfficherMesTrocs";
		
	}
		
	@RequestMapping(method = RequestMethod.GET, value = { "/TROC" })
	public String afterlog(Model model) {
				
		return "TROC";
		
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = { "/Contact" })
	public String contact(Model model) {
				
		return "Contact";
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = { "/parametres" })
	public String parametres(Model model) {
				
		return "parametres";
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = { "/questrep" })
	public String questrep(Model model) {
				
		return "questrep";
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = { "/do_deconnection" })
	public String index(HttpSession session ) {
		session.removeAttribute(AccueilController.CONNECTED_USER);
		return "index";
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = { "/A_propos_de_Nous" })
	public String A_propos_de_Nous(Model model) {
				
		return "A_propos_de_Nous";
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = { "/ou_sommes_nous" })
	public String ou_sommes_nous(Model model) {
				
		return "ou_sommes_nous";
				
	}
	
	@RequestMapping(method = RequestMethod.GET, value = { "/dons" })
	public String dons(Model model) {
				
		return "dons";
				
	}
	
	@RequestMapping(method = RequestMethod.GET, value = { "/reponse" })
	public String reponse(Model model) {
				
		return "reponse";
				
	}
	

//	
//	@RequestMapping(method = RequestMethod.GET, value = { "/touslesmembres" })
//	public String touslesmembres(Model model) {
//				
//		return "touslesmembres";
//				
//	}
	
	
	
	
}
