package tdb.grp3.trocndon.controller;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import tdb.grp3.trocndon.dao.TrocDao;
import tdb.grp3.trocndon.form.TrocForm;
import tdb.grp3.trocndon.model.Membre;
import tdb.grp3.trocndon.model.Troc;
import tdb.grp3.trocndon.model.User;
import tdb.grp3.trocndon.service.FileSystemStorageService;

@Controller
public class TrocController {
	
	@Autowired
	TrocDao trocDao;
	@Autowired
	private FileSystemStorageService storageService;
	
	public static final String CONNECTED_USER = "connectedUser";
	public static final String TROC_EN_COURS = "troc en cours";

	
	@RequestMapping(method = RequestMethod.POST, value = { "/doTrocoudon" })
	public String trocoudon(HttpSession session ,Model model, TrocForm form) {
		
		Troc troc = new Troc();
		User donneur = (User) session.getAttribute(CONNECTED_USER);
		troc.setDonneur(donneur.getMembre());
		troc.setDateAction(new Date());
		troc.setTitre(form.getTitre());
		troc.setType(form.getType());
		troc.setDescription(form.getDescription());
		
		trocDao.save(troc);
		session.setAttribute(TROC_EN_COURS, troc);
		
		return "redirect:/trocoudons";
		
	}
	
	@PostMapping("doAjouterPhotoTroc")
	public String ajouterPhotoTroc(MultipartFile file, HttpSession session) {
		
		Troc troc = (Troc) session.getAttribute(TROC_EN_COURS);
		
		Path chemin = storageService.storeAndGetPath(file,Paths.get(troc.getId()+"/imageTroc"));
		
		
		troc.setPhotoTroc(chemin.getFileName().toString());
		
		trocDao.save(troc);
		
		
		return "redirect:/afterlog";
		
	}
	
	@GetMapping("/files/{idTroc}/imageTroc/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> getImage(@PathVariable String idTroc,@PathVariable String filename) {
		
		Resource file = storageService.loadAsResource(filename,idTroc+"/imageTroc");
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}
	
	@GetMapping("/trocoudons")
	public String consulterLesTroc(Model model) {

		List<Troc> trocs = trocDao.findAll();
		model.addAttribute("trocs", trocs);
		return "TROC";
	}
	
	
	@GetMapping("/afficherContacterTrockeur")
	public String contactertrockeur(Model model,Long idTroc) {
		Troc troc = trocDao.findById(idTroc).get();
		
		model.addAttribute("troc", troc);
		return "contactertrockeur";
	}
	
	@GetMapping("/doAfficherMesTrocs")
	public String afficherMesTrocs(HttpSession session, Model model) {
		User user = (User) session.getAttribute(CONNECTED_USER);
		Membre donneur = new Membre();
		
		
		List<Troc> trocs = trocDao.recupererLesTrocUser(user.getMembre().getId());
		model.addAttribute("trocs", trocs);
//		trocs = user.getMembre().getTrocs();
		
		return "mestrocks";
	}
	

	
}
