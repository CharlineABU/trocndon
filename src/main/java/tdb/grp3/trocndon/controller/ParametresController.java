package tdb.grp3.trocndon.controller;

import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import tdb.grp3.trocndon.dao.UserDao;
import tdb.grp3.trocndon.form.ParametresForm;
import tdb.grp3.trocndon.model.User;
import tdb.grp3.trocndon.service.FileSystemStorageService;

@Controller
public class ParametresController {

	@Autowired
	UserDao userDao;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private FileSystemStorageService storageService;

	public static final String CONNECTED_USER = "connectedUser";

	// chercher le membre et l'afficher sur IHM parametre
	@RequestMapping(method = RequestMethod.GET, value = { "/doAfficherUnMembre" })
	public String afficherUnMembre() {
		return "parametres";
	}

	// Modifier les informations
	@RequestMapping(method = RequestMethod.POST, value = { "/doModifierUnMembre" })
	public String modifierUnMembre(HttpSession session, Model model, ParametresForm form) {

		User connectedUser = (User) session.getAttribute(CONNECTED_USER);

		connectedUser.getMembre().setNom(form.getNom());
		connectedUser.getMembre().setPrenom(form.getPrenom());
		connectedUser.setEmail(form.getEmail());
		String pswEncoder = passwordEncoder.encode(form.getPassword());
		connectedUser.setMotDePasse(pswEncoder);

		userDao.save(connectedUser);

		model.addAttribute("messageOk", "La modification est ok");

		return "redirect:/afterlog";

	}

	@PostMapping("doModifierPhotoProfil")
	public String modifierPhotoProfile(MultipartFile file, HttpSession session) {

		User connectedUser = (User) session.getAttribute(CONNECTED_USER);

		Path chemin = storageService.storeAndGetPath(file,Paths.get(connectedUser.getId()+"/image"));

		
		connectedUser.setPhotoProfil(chemin.getFileName().toString());

		userDao.save(connectedUser);

		return "redirect:/parametres";

	}

	@GetMapping("/files/{idUser}/image/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> getImage(@PathVariable String idUser,@PathVariable String filename) {

		Resource file = storageService.loadAsResource(filename,idUser+"/image");
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}


}
