package tdb.grp3.trocndon.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tdb.grp3.trocndon.dao.MembreDao;
import tdb.grp3.trocndon.dao.TrocDao;
import tdb.grp3.trocndon.dao.UserDao;
import tdb.grp3.trocndon.form.InscriptionForm;
import tdb.grp3.trocndon.form.LoginForm;
import tdb.grp3.trocndon.model.Membre;
import tdb.grp3.trocndon.model.Troc;
import tdb.grp3.trocndon.model.User;

@Controller
public class AccueilController {

    public static final String CONNECTED_USER = "connectedUser";

    @Autowired
    private MembreDao          membreDao;
    @Autowired
    private UserDao            usersDao;
    @Autowired
    private TrocDao            trocDao;
    @Autowired
    private PasswordEncoder    passwordEncoder;
    //@Autowired
    //	private StorageProperties storageProperties;

    @RequestMapping(method = RequestMethod.GET, value = { "/", "/index" })
    public String acceuil() {
        return "index";
    }

    @RequestMapping(method = RequestMethod.POST, value = { "/do_inscription" })
    public String inscription(HttpSession session, InscriptionForm form) {

        Membre membre = new Membre();
        membre.setNom(form.getNom());
        membre.setPrenom(form.getPrenom());
        membre.setDateNaissance(form.getBirth());
        membre.setSexe(form.getSexe());

        User user = new User();

        user.setEmail(form.getEmail());
        String pswEncoder = passwordEncoder.encode(form.getPassword());
        user.setMotDePasse(pswEncoder);
        user.setMembre(membre);

        User leUser = usersDao.save(user);

        session.setAttribute(CONNECTED_USER, leUser);
        return "afterlog";
    }

    @RequestMapping(method = RequestMethod.POST, value = { "/do_connexion" })
    public String connexion(HttpSession session, Model model, LoginForm form) {

        User user = usersDao.findByEmail(form.getEmail());

        if (user == null) {
            model.addAttribute("message", "LE MAIL EST INCORRECT");
            return "index";
        }
        if (!passwordEncoder.matches(form.getPassword(), user.getMotDePasse())) {
            model.addAttribute("message", "LE MOT DE PASSE EST INCORRECT");
            return "index";
        }

        session.setAttribute(CONNECTED_USER, user);

        List<Troc> trocs = trocDao.findAll();
        model.addAttribute("trocs", trocs);

        return "afterlog";
    }

}
