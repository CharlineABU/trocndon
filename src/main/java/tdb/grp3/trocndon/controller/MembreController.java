package tdb.grp3.trocndon.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import tdb.grp3.trocndon.dao.MembreDao;
import tdb.grp3.trocndon.dao.UserDao;
import tdb.grp3.trocndon.model.User;


@Controller
public class MembreController {
	
	@Autowired
	MembreDao membreDao;
	@Autowired
	UserDao usersDao;
	

	// afficher tous les membres et leur email
	
	@GetMapping("/touslesmembres")
	public String touslesmembres(Model model) {
		
		List<User> users = usersDao.findAll();
		model.addAttribute("users", users);

		return "touslesmembres";
	}
	

	
}
