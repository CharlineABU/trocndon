package tdb.grp3.trocndon.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tdb.grp3.trocndon.dao.TrocDao;
import tdb.grp3.trocndon.form.ContactForm;
import tdb.grp3.trocndon.form.ContactezTrockeurForm;
import tdb.grp3.trocndon.model.Troc;
import tdb.grp3.trocndon.model.User;
import tdb.grp3.trocndon.properties.EmailProperties;
import tdb.grp3.trocndon.service.EmailService;

@Controller
public class ContactController {

	public static final String CONNECTED_USER = "connectedUser";
	@Autowired
	TrocDao trocDao;
	@Autowired
	private EmailProperties emailProperties;
	@Autowired
	public EmailService emailService;

	@PostMapping("/doEnvoyerMail")
	public String envoyerMail(ContactForm form, HttpSession session, RedirectAttributes attributes) {
		String messageComplet = "Message envoyé par : ";
		String emailBy = null;
		User user = (User) session.getAttribute(CONNECTED_USER);
		emailBy = user.getEmail();
		messageComplet += emailBy + "(" + user.getMembre().getNom() + " " + user.getMembre().getPrenom() + ") \n";

		messageComplet += "message Envoyé : \n" + form.getMessage();
		emailService.sendSimple("trocndon@gmail.com", form.getObjet(), messageComplet);
		messageComplet = "Nous avons bien reçu vôtre mail, nous reviendrons vers vous dans les plus bref delais. Merci. \n"
				+ messageComplet;
		emailService.sendSimple(emailBy, emailProperties.getMailNoReplay(), "AR de votre demande de contact",
				messageComplet);

		attributes.addFlashAttribute("messageSucces", "Mail envoyé avec succes, l'Administrateur vous repondra dans les plus brefs delais !");

		return "redirect:/affContact";
		
	}
	
	@GetMapping("/affContact")
	public String affContact(RedirectAttributes attributes) {
		
		attributes.addFlashAttribute("messageSucces", "Mail envoyé avec succes, l'Administrateur vous repondra dans les plus brefs delais !");

		return "Contact";
		
	}

	@PostMapping("/doContactezTrockeur")
	public String contactezTrockeur(HttpSession session, RedirectAttributes attributes, ContactezTrockeurForm form) {

		String messageComplet = "Message envoyé par : ";
		String emailFrom = null;
		String emailTo = null;

		User user = (User) session.getAttribute(CONNECTED_USER);
		emailFrom = user.getEmail();
		messageComplet += emailFrom + "(" + user.getMembre().getNom() + " " + user.getMembre().getPrenom() + ") \n";
		Troc troc = trocDao.findById(form.getId()).get();
		emailTo = troc.getDonneur().getUser().getEmail();
		messageComplet += "A : " + emailTo + "(" + troc.getDonneur().getNom() + " " + troc.getDonneur().getPrenom()
				+ ")\n";

		messageComplet += "message Envoyé : \n" + form.getMessage();
		// message principal
		emailService.sendSimple(emailTo, emailFrom, form.getObjet(), messageComplet);
		// une copie pour l'administrateur
		emailService.sendSimple(emailProperties.getMailReplay(), "copie : " + form.getObjet(), messageComplet);
		messageComplet = "Votre mail à bien éte envoyé, le Trockeur reviendra vers vous dans les plus bref delais. Merci. \n"
				+ messageComplet;

		emailService.sendSimple(emailFrom, emailProperties.getMailNoReplay(), "AR de votre demande de contact",
				messageComplet);

		attributes.addFlashAttribute("messageSucces", "Mail envoyé avec succes, le Trockeur vous repondra dans les plus brefs delais !");

		return "redirect:/afficherContacterTrockeur?idTroc=" + troc.getId();
	}

	@RequestMapping(value = "/sendMimeEmail", method = RequestMethod.GET)
	public String sendMimeEmail() {

		String text = "Dear %s, Mail Content : %s";

		// public void sendMime(String dear, String content, String to, String subject,
		// String text,
		emailService.sendMime("Mireille", "Hello, Im testing mime Email", emailProperties.getMailReplay(),
				"Test Simple Email", text, "C:/toto.pdf");

		return "index";
	}

}
