package tdb.grp3.trocndon.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import tdb.grp3.trocndon.dao.QuestionDao;
import tdb.grp3.trocndon.dao.ReponseDao;
import tdb.grp3.trocndon.form.ReponseForm;
import tdb.grp3.trocndon.model.Question;
import tdb.grp3.trocndon.model.Reponse;
import tdb.grp3.trocndon.model.User;

@Controller
public class QuestrepController {

	@Autowired
	ReponseDao reponseDao;
	@Autowired
	QuestionDao questionDao;

	public static final String CONNECTED_USER = "connectedUser";

	@PostMapping("/doPoserUneQuestion")
	public String doPauserUneQuestion(HttpSession session, Model model,
		@RequestParam(required = true, name = "question") String questionParam) {

		Question question = new Question();
		User poseur = (User) session.getAttribute(CONNECTED_USER);
		question.setPoseur(poseur.getMembre());
		question.setDateQuestion(new Date());
		questionParam = questionParam.replaceAll("\\r\\n", "<br/>");
		question.setQuestion(questionParam);

		questionDao.save(question);

		return "redirect:/poserUneQuestion";
	}

	@GetMapping("/poserUneQuestion")
	public String poserUneQuestion(Model model) {

		List<Question> questions = questionDao.findAll();

		model.addAttribute("questions", questions);
		return "questrep";
	}

	@GetMapping("/reponses")
	public String afficherReponses(Model model, Long idQuestion) {

		Question question = questionDao.findById(idQuestion).get();

		model.addAttribute("question", question);
		return "reponse";
	}

	@PostMapping("/doDonnerUneReponse")
	public String doDonnerUneReponse(HttpSession session, Model model, ReponseForm form) {

		Question question = questionDao.findById(form.getIdQuestion()).get();
		Reponse reponse = new Reponse();

		reponse.setDateReponse(new Date());
		reponse.setReponse(form.getReponse());
		User repondeur = (User) session.getAttribute(CONNECTED_USER);
		reponse.setRepondeur(repondeur.getMembre());
		reponse.setQuestion(question);

		reponseDao.save(reponse);

		return "redirect:/reponses?idQuestion=" + form.getIdQuestion();
	}
	
	@GetMapping("/doAfficherListeDesQuestions")
	public String afficherListeDesQuestions(Model model) {

		List<Question> questions = questionDao.findAll();

		model.addAttribute("questions", questions);
		return "listedesquestions";
		
	}

}
