package tdb.grp3.trocndon.conf;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationFilterConfig {

	/**
	 * Check if user is logged in
	 */
	@Bean
	public FilterRegistrationBean<LoginFilter> applicationFilterRegistration() {
		// created a collections that need logins to be validated.
		List<String> excludesUrlPatterns = new ArrayList<String>();
		excludesUrlPatterns.add("/index");
		excludesUrlPatterns.add("/");
		excludesUrlPatterns.add("/do_connexion");
		excludesUrlPatterns.add("/do_inscription");
		excludesUrlPatterns.add("/css/");
		excludesUrlPatterns.add("/fonts/");
		excludesUrlPatterns.add("/js/");
		excludesUrlPatterns.add("/img/");
		excludesUrlPatterns.add("/upload");

		LoginFilter loginFilter = new LoginFilter(excludesUrlPatterns);

		FilterRegistrationBean<LoginFilter> registration = new FilterRegistrationBean<LoginFilter>();
		registration.setFilter(loginFilter);
		registration.setOrder(1);
		return registration;

	}

}