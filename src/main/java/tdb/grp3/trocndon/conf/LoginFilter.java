package tdb.grp3.trocndon.conf;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tdb.grp3.trocndon.controller.AccueilController;
import tdb.grp3.trocndon.model.User;

public class LoginFilter implements Filter {

	private List<String> excludesPatterns;

	public LoginFilter() {
		super();
	}

	public LoginFilter(List<String> excludesPatterns) {
		super();
		this.excludesPatterns = excludesPatterns;
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterchain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpSession session = httpRequest.getSession(false);
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		System.out.println("URI: " + httpRequest.getRequestURI());
		User connectedUser = null;
		if (session != null)
			connectedUser = (User) session.getAttribute(AccueilController.CONNECTED_USER);

		if (!isExcluded(httpRequest.getRequestURI()) && connectedUser == null) {
			String contextPath = httpRequest.getContextPath();
			httpResponse.sendRedirect(contextPath + "/index");
		} else {
			filterchain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig filterconfig) throws ServletException {
	}

	public Boolean isExcluded(String url) {
		if (excludesPatterns == null)
			return false;

		for (String string : excludesPatterns) {
			if (url.equalsIgnoreCase(string)
					|| (string.length() > 1 && url.toLowerCase().startsWith(string.toLowerCase()))) {
				return true;
			}
		}

		return false;
	}

	public List<String> getExcludesPatterns() {
		return excludesPatterns;
	}

	public void setExcludesPatterns(List<String> excludesPatterns) {
		this.excludesPatterns = excludesPatterns;
	}
}