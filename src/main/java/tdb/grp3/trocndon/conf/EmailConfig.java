package tdb.grp3.trocndon.conf;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import tdb.grp3.trocndon.properties.EmailProperties;

@Configuration
public class EmailConfig {
	
	@Autowired
	private EmailProperties emailProperties;
	
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(emailProperties.getMailHost());
        mailSender.setPort(emailProperties.getMailPort());
        mailSender.setProtocol("smtp");
        mailSender.setUsername(emailProperties.getMailUserName());
        mailSender.setPassword(emailProperties.getMailPassword());
 
        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", emailProperties.getMailTransportProtocol());
        props.put("mail.smtp.auth", emailProperties.getMailSmtpAuth());
        props.put("mail.smtp.starttls.enable", emailProperties.getMailSmtpStartTlsEnable());
        props.put("mail.debug", emailProperties.getMailDebug());
     
 
        return mailSender;
    }

}
