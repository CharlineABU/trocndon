package tdb.grp3.trocndon.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import tdb.grp3.trocndon.properties.EmailProperties;

@Service
public class EmailService {
	
	@Autowired
	private EmailProperties emailProperties;
	@Autowired
	public JavaMailSender emailSender;

	public Boolean sendSimple(SimpleMailMessage message) {
		try {

			// Send Message!
			this.emailSender.send(message);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public Boolean sendSimple(String to, String from, String subject, String text) {
		try {
			SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(to);
			message.setSubject(subject);
			message.setText(text);
			if (from != null) {
				message.setFrom(from);
				message.setReplyTo(from);
			}
			// Send Message!
			this.emailSender.send(message);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public Boolean sendSimple(String to, String subject, String text) {
		return sendSimple(to, null, subject, text);
	}

	/*
	 * Dear %s, Mail Content : %s
	 */

	public void sendMime(String dear, String content, String to, String subject, String text, String pathFile) {

		MimeMessage message = emailSender.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setFrom(emailProperties.getMailReplay());
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(String.format(text, dear, content));

			FileSystemResource file = new FileSystemResource(pathFile);
			helper.addAttachment(file.getFilename(), file);
			emailSender.send(message);
		} catch (MessagingException e) {
			throw new MailParseException(e);
		}

	}

	public void sendMime(MimeMessage message) {

		emailSender.send(message);
	}

}
