package tdb.grp3.trocndon.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import tdb.grp3.trocndon.exception.StorageException;
import tdb.grp3.trocndon.exception.StorageFileNotFoundException;
import tdb.grp3.trocndon.properties.StorageProperties;

@Service
public class FileSystemStorageService {

	private final Path rootLocation;

	@Autowired
	public FileSystemStorageService(StorageProperties properties) {
		this.rootLocation = Paths.get(properties.getLocationDir());
	}

	public void store(MultipartFile file) {
		String filename = StringUtils.cleanPath(file.getOriginalFilename());
		try {
			if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file " + filename);
			}
			if (filename.contains("..")) {
				// This is a security check
				throw new StorageException(
						"Cannot store file with relative path outside current directory " + filename);
			}
			InputStream inputStream = file.getInputStream();
			Files.copy(inputStream, this.rootLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING);

		} catch (IOException e) {
			throw new StorageException("Failed to store file " + filename, e);
		}
	}

	public Path storeAndGetPath(MultipartFile file) {
		return storeAndGetPath(file, null);

	}

	public Path storeAndGetPath(MultipartFile file, Path chemin) {
		String filename = StringUtils.cleanPath(file.getOriginalFilename());
		try {
			if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file " + filename);
			}
			if (filename.contains("..")) {
				// This is a security check
				throw new StorageException(
						"Cannot store file with relative path outside current directory " + filename);
			}
			InputStream inputStream = file.getInputStream();
			Path pathFinal = null;
			if (chemin == null) {
				pathFinal=this.rootLocation;
			} else {
				pathFinal=this.rootLocation.resolve(chemin);
			}
			if(Files.notExists(pathFinal)) {
				Files.createDirectories(pathFinal);
			}
			pathFinal = pathFinal.resolve(filename);
			Files.copy(inputStream, pathFinal, StandardCopyOption.REPLACE_EXISTING);
			return pathFinal;
		} catch (IOException e) {
			throw new StorageException("Failed to store file " + filename, e);
		}

	}

	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.rootLocation, 1).filter(path -> !path.equals(this.rootLocation))
					.map(this.rootLocation::relativize);
		} catch (IOException e) {
			throw new StorageException("Failed to read stored files", e);
		}

	}

	public Path load(String filename) {
		return load(filename, null);
	}

	public Path load(String filename, String chemin) {
		if (chemin == null || chemin.isEmpty())
			return rootLocation.resolve(filename);
		else
			return rootLocation.resolve(chemin).resolve(filename);
	}

	public Resource loadAsResource(String filename) {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new StorageFileNotFoundException("Could not read file: " + filename);

			}
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}
	
	public Resource loadAsResource(String filename,String chemin) {
		try {
			
			Path file = null;
			if(chemin==null || chemin.isEmpty()) {
				file=load(filename);
			}else{
				file=load(filename,chemin);
			}
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new StorageFileNotFoundException("Could not read file: " + filename);

			}
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}

	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

	public void init() {
		try {
			Files.createDirectories(rootLocation);
		} catch (IOException e) {
			throw new StorageException("Could not initialize storage", e);
		}
	}
}
