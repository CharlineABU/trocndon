package tdb.grp3.trocndon.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tdb.grp3.trocndon.model.Troc;


@Repository
public interface TrocDao extends JpaRepository<Troc, Long> {
	
	public List<Troc> findByTitreAndType(String titre,String type);
	
	@Query("SELECT t from Troc t where t.donneur.id=:idMembre")
	public List<Troc> recupererLesTrocUser(@Param("idMembre") Long idMembre);
	
//	@Query("SELECT t from Troc t where t.donneur.id=1? and autre champ=2?")
//	public List<Troc> recupererLesTrocUser(Long idMembre);
}
