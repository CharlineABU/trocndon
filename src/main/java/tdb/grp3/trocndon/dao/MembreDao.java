package tdb.grp3.trocndon.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tdb.grp3.trocndon.model.Membre;

@Repository
public interface MembreDao extends JpaRepository<Membre, Long> {

}
