package tdb.grp3.trocndon.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tdb.grp3.trocndon.model.User;

@Repository
public interface UserDao extends JpaRepository<User, Long>{
	
	public User findByEmailAndMotDePasse(String email,String pwd);

	public User findByEmail(String email);
}
