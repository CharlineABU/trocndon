$().ready(function() {
	$("#parameterForm").validate({
		rules : {
			password : {
				required : true,
				minlength : 6,
				maxlength : 10

			},

			passwordConfirm : {

				equalTo : "#password",
				minlength : 6,
				maxlength : 10
			}

		},

		messages : {
			passwordConfirm : "Veuillez saisir un mdp identique"

		}

	});
});